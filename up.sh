#!/usr/bin/env bash

####################################################################################
###                        GLOBAL VARIABLES                                      ###
####################################################################################

BASEDIR=$(readlink -f "$(dirname "$0")") #absolut path

####################################################################################
###                        OPTIONS & ARGUMENTS                                   ###
####################################################################################
BACKEND_PORT=""
FRONTEND_PORT=""
HOST_IP="10.1.15.162" 

usage(){
  echo "Inicia el backend y el frontend de Hearbeat2"
  echo "USAGE:" "./up.sh --host ${HOST_IP} --backend-port 5047 --frontend-port 5048"
  exit
}

while true; do
  case "$1" in
    -b | --backend-port ) BACKEND_PORT=$2; shift 2;;
    -f | --frontend-port ) FRONTEND_PORT=$2; shift 2;;
    -i | --host ) HOST_IP=$2; shift 2;;
    -h | --help ) usage; shift 1;;
    * ) break ;;
  esac
done


#comprobar que haya pasado los parametros necesarios
if [ -z "${BACKEND_PORT}" ] || [ -z "${FRONTEND_PORT}" ] || [ -z "${HOST_IP}" ] ; then
     echo "Faltan parametros para procesar la solicitud (--backend-port y --frontend-port son requeridos)"
     exit 1 #<- esto es fundamental
fi

####################################################################################
###                          FUNCTIONS                                           ###
####################################################################################
# log : Escribe mensajes por salida estandar, de acuerdo al nivel de log que se haya
#usage log "TRACE" "Comprobando SIGAE INTERNO" 
LOG_LEVEL="INFO"
declare -A LOGS_LEVELS
LOGS_LEVELS=(["ERROR"]=1 ["WARNING"]=2 ["INFO"]=3 ["TRACE"]=4)

log(){
  local entry_level="$1"
  local msg="$2"

  if [ ${LOGS_LEVELS[${entry_level}]} -le ${LOGS_LEVELS[${LOG_LEVEL}]} ] ; then
    echo [`date '+%Y-%m-%dT%H:%M:%S'`]"[$entry_level]" "$msg"
  fi
}


start_backend(){
  log "TRACE" "Starting heartbeat backend at $${BACKEND_PORT}"
  cd ${BASEDIR}/server && forever start src/heartbeat.js --port ${BACKEND_PORT}
}
start_frontend(){
  log "TRACE" "Starting heartbeat frontend at ${FRONTEND_PORT}"
  cd ${BASEDIR}/heartbeat-frontend/ 
  source ./start.sh --port ${FRONTEND_PORT} --ws-ip ${HOST_IP} --ws-port ${BACKEND_PORT} 
}


####################################################################################
###                             MAIN                                             ###
####################################################################################

start_backend
start_frontend


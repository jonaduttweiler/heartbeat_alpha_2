/**
 * @author Jonatan E. Duttweiler <jduttweiler@santafe.gov.ar>
 */
const path = require('path');
const app = require('express')();
const http = require('http');
const server = http.Server(app);
const io = require('socket.io')(server);

/*############################################################################
 	MODULES IMPORTS
############################################################################*/

const root_path = path.dirname(__dirname);//must be /project or /project/server
const get_global_options = require(`${root_path}/lib/options_reader.js`).get_global_options;
const get_instances_to_check = require(`${root_path}/lib/instances_reader.js`).get_instances_to_check;
const database_handler = require(`${root_path}/lib/database_handler.js`);
const notify_delay = require(`${root_path}/lib/api_notifier.js`).notify_delay;

const makeRequest = require(`${root_path}/lib/http_helpers`).makeRequest;
const check_assertions = require(`${root_path}/lib/assertions_helpers`).check_assertions;
const log = require(`${root_path}/lib/logger.js`).logger_log;

const Request = require(`${root_path}/model/Request.js`);



/*############################################################################
 	IMPORT OPTIONS
############################################################################*/
const global_options = get_global_options();
const instances_to_check = get_instances_to_check();


/*############################################################################
 	Local variables
############################################################################*/
var check_interval_ms = global_options.config_check_interval_ms;
var interval_timer_id;


/*############################################################################
 	INIT MONGODB && LISTEN HTTP
############################################################################*/
var db;
async function init(){
	try{
		if(global_options.record_db){
			db = await database_handler.connect(global_options);
		}

		server.listen(global_options.app_port, function(){
  			log("BOOTSTRAP","APP",`Heartbeat2 listening on ${global_options.app_port}`);
  			setCheckInterval(check_interval_ms);
		});

	} catch(err){
		
		if(err.name && err.errmsg){
			log("FATAL","${err.name}",`${err.errmsg}`);	
		}
		
		console.log(err);
		process.exit();
	}
}
init();


/*############################################################################
 	Hook events requests lifecycle
############################################################################*/
Request.SET_HANDLER("sent",function(request){
	if(global_options.print_in_console){
		//console.log(request.toString());
	}
	notify_connected_users(request);
});


/**
	Funcion que establece el intervalo de comprobacion para todas las instancias
	@param ms_interval: number intervalo en milisegundos para setear el timer

	@return timer_id:number
*/
function setCheckInterval(ms_interval){
	log("INFO","APP",`Set interval ${ms_interval} ms`);

	if(interval_timer_id){
		clearInterval(interval_timer_id);
	}

	let timer_id = setInterval(() => {
		check_all_instances();
	}, ms_interval);

	interval_timer_id = timer_id;
	check_interval_ms = ms_interval;

	return timer_id;
}


//start 
function check_all_instances(){
	log("TRACE","APP",`Checking all instances`);
	for(let instance of instances_to_check){
		check_instance(instance); 
	}
}

/** 
	check_instance
	Realiza el request, guarda el resultado en mongo, notifica a los usuarios conectados 
*/

async function check_instance(instance){
	try{
		let request = await makeRequest(new Request(instance));	

		// Estas funciones dependen de lo que se haya definido en las opciones
		log_request(request);
		check_assertions(request);
		notify_delay(request); 

		//estas acciones si son fundamentales
		store_request(request); 
		notify_connected_users(request); 

	} catch(err){
		console.log(err);
	}
}



/**
	Best effort approach
*/
async function store_request(request){
	let request_to_insert = {...request}; //clone

	if(global_options.record_db && db){

		//Eliminamos datos que no es necesario almacenar 
		delete request_to_insert.id;
		delete request_to_insert.url;

		if(!global_options.store_all_properties){	
			delete request_to_insert.port; 
			delete request_to_insert.status; 
			delete request_to_insert.cookies;
			delete request_to_insert.cookies_obj;
			delete request_to_insert.path;
			delete request_to_insert.response_headers;

			if(request_to_insert.cookies==""){
				delete request_to_insert.cookies;
			}
		}

		if(request_to_insert.summary==""){
			delete request_to_insert.summary;
		}


		//"match_assertions" : true,
		//"assertions_cause" : ""
		//cookies_obj si es un objeto vacio borrarlo

		delete request_to_insert.response_data; 

		return await db.collection('requests').insertOne(request_to_insert);
	} else if(!db){
		//console.warn("STORE: No Mongo db available");
	}
}

function log_request(request){ 
	if(global_options.print_in_console){
		//log("TRACE","REQUEST",`${request.toString()}`);
	}
}


/**
	Send notifications to connected users
*/
function notify_connected_users(request){
	if(g_socket){
		g_socket.emit("request_update",request);
	} 	
}


/*############################################################################
 	API REST
############################################################################*/
//TODO: incluir algo de informacion en la pagina de test, como el periodo de comprobacion
//conexion a la bd y alguna que otra cosa
app.get('/', function(req, res){
  res.send(`<h1>Heartbeat 2 backend- Test page </h1>`); 
});




/*############################################################################
 	WEB SOCKETS
############################################################################*/
var g_socket =  null;  //Expose socket as global
var connected_users = 0;
io.on('connection', function(socket){
  connected_users++;
  log("TRACE","WEB SOCKET",`${connected_users} connected users`);

  g_socket = socket;

  socket.on('message_test', function(payload){
	log("INFO","WEB SOCKET",`[message_test]:${payload}`);
  	socket.emit('response_test',{"response":"payload"});
  });

  socket.on('disconnect', function () {
	  connected_users--;
	  log("TRACE","WEB SOCKET",`${connected_users} connected users`);
  });


  socket.on('check_now',function(payload){
  	 check_all_instances();
  });

  socket.on('get_check_interval',function(payload){
  	socket.emit("get_check_interval_awk",{check_interval_ms});	 
  });

 
  socket.on('set_check_interval',function(payload){
  	if(payload.is_admin && payload.ms_interval >= global_options.check_interval_min_ms){
  		setCheckInterval(payload.ms_interval);
  		console.log(`[CHECK INTERVAL] Update:${check_interval_ms}`);
  		socket.emit('set_check_interval_awk',true);
  	} else {
  		console.log("[CHECK INTERVAL] Out of range or not admin!");
  		socket.emit('set_check_interval_awk',false);
  	}
  	
  });
  	

});

const path = require('path');
const MongoClient = require('mongodb').MongoClient;

const root_path = path.dirname(__dirname);
const log = require(`${root_path}/lib/logger.js`).logger_log;
/*
	Returns db object
*/
async function connect(global_options){

	log_info_database(global_options);
	
	let client = await MongoClient.connect(global_options.mongo_db_url,{ useNewUrlParser: true });
	db = client.db(global_options.database_name); 
	
	//check connection?
    await db.collection("requests").findOne({});
	return db;
}


function log_info_database(global_options){

	log("BOOTSTRAP","MONGO",`Storing at ${global_options.database_endpoint}/${global_options.database_name}`);
    log("BOOTSTRAP","MONGO",`Store all properties: ${global_options.store_all_properties}`);
}


module.exports.connect = connect; 
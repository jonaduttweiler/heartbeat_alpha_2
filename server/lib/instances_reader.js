const path = require('path');
const fs = require('fs');
const root_path = path.dirname(__dirname);//must be /project or /project/server

/**Devuelve un objeto con las opciones globales de la aplicacion*/
function get_instances(){ 
	//open file
	let instances_to_check = JSON.parse(fs.readFileSync(`${root_path}/assets/instances_to_check.json`, 'utf8'));
	//check some
	if(valid_instances(instances_to_check)){
		return instances_to_check;	
	} 	
}


function valid_instances(instances_to_check){
	//console.log("Validating instances...");
	return true;
}

module.exports.get_instances_to_check = get_instances; 
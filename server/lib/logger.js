//Usage: log("INFO","MONGODB","DB connected!");
const LEVELS = [
	"FATAL",
	"BOOTSTRAP",
	"ERROR",
	"SEVERE",
	"WARNING",
	"INFO",
	"TRACE"
];

const threshold_idx = 5; //HASTA INFO, pero no trace

function logger_log(level,module,msg,url=""){
	let now = new Date().toISOString();

	if(LEVELS.indexOf(level) <= threshold_idx){
		console.log(`[${now}][${level}][${module}] ${msg} ${url}`);	
	}
}

module.exports.logger_log = logger_log; 
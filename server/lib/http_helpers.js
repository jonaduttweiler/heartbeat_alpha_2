var http = require('http');
var https = require('https');

/**
	Return object with data needed to make the request
*/	
function generate_options(request){

	//Generate request headers
	let headers = {};

	if(request.cookies){
		headers['Cookie'] = request.cookies;
	}

	if(true){ //TODO: Refact, this depends of the request, should be specified in instances_to_check file
		headers['Content-Type'] = "application/json";
	}
	
	/*bearer token*/
	if(request.bearer_token){
		headers['Authorization'] ='Bearer <'+request.bearer_token+'>';
	}

	let options = {...request,headers:headers}; 
	
	delete options.protocol;
	return options;
}


/**
	Return the module to use for make requests (http or https)
*/	
function determine_network_protocol(request){

	
	if(request.protocol==="https" || !request.protocol){ //default https
		return https;
	} else if(request.protocol === "http"){
		return http;
	}

}


/**
	Funcion that makes an http/https request
	@param request: Objeto request que contiene(protocol,host,port,path,cookies) de la request a realizar
	This function modify the request received as parameter
	@returns promise. 
		Fullfil (request + datos response , pero se agregan al request de todas formas)
		Reject (error)
								
*/
function makeRequest(request){
	
	return new Promise((resolve,reject) => {

	 	let response_text ="";	
		let stamp1,stamp3,stamp_lookup,stamp_tcpConnectionAt,stamp_tlsHandshakeAt,stamp_readable;

		let options = generate_options(request);
		
		stamp1 = new Date();
		request.mark_as_sent(stamp1);

		let network_module = determine_network_protocol(request);

		var req = network_module.get(options, function(res) {
		   request.process_response_headers(res);

		   //firstByteAt
	  	    res.on('readable', () => {
			   stamp_readable = new Date();
		    });

		    res.on('data', function (chunk) {
		        response_text += chunk;
		    });
		    res.on('end', function () {
		        stamp3 = new Date();

				let timing = {stamp1,stamp3,stamp_lookup,stamp_tcpConnectionAt,stamp_tlsHandshakeAt,stamp_readable};
				request.calculate_timing(timing);

		        try {
			        request.response_data = JSON.parse(response_text);
			    } catch (e) {
			        request.response_data = response_text;
			    }
			    request.process_response_data();
			    resolve(request);
		    });
		});


		//Socket timing
		req.on('socket', (socket) => {
			socket.on('lookup', () => {
			  stamp_lookup = new Date();
			});
			socket.on('connect', () => {
			  stamp_tcpConnectionAt = new Date();
			});
			socket.on('secureConnect', () => {
			  stamp_tlsHandshakeAt = new Date();
			});
	  	});

		//Connection error
		req.on('error', function(e) {
	        request.register_connection_error(e);
	        reject("ERROR::"+request.summary+"("+request.host+")");
		});

		req.end();
	});
};


/* ---------------------------------------------------------------------------------------*
	MODULE EXPORTS
   ---------------------------------------------------------------------------------------*/

	module.exports.makeRequest =  makeRequest;

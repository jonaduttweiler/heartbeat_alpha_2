const path = require('path');
const fs = require('fs');
const root_path = path.dirname(__dirname);
const log = require(`${root_path}/lib/logger.js`).logger_log;


let global_options;
/**Devuelve un objeto con las opciones globales de la aplicacion*/
function get_global_options(){
	if(global_options == undefined) {
		global_options = parse_global_options();
	}	
	return global_options;	
}


function parse_global_options(){ 
	//open file
	//log("TRACE","BOOTSTRAP",`Reading file options...`);
	let global_options = JSON.parse(fs.readFileSync(`${root_path}/config/options.json`, 'utf8'));

	/*Podemos sobreescribir el puerto por defecto desde la linea de comandos*/
	let port_command_line = get_port_command_line();
	if(port_command_line > 0 ){
		global_options.app_port = port_command_line;
	}

	//Por defecto almacenamos todas las propiedades
	if(global_options.store_all_properties === undefined){
		global_options.store_all_properties = true;
	}

	//check some
	if(valid_options(global_options)){

		//Derived properites
		global_options.database_name = global_options.mongo_db_url.split("/").slice(-1)[0];

		global_options.database_endpoint = global_options.mongo_db_url.includes("@")?
													global_options.mongo_db_url.split("@")[1].split("/")[0]
												   :global_options.mongo_db_url.split("/").slice(-2)[0];


		return global_options;	
	} 	
}


//override options from command line
/**
	Return number if exists
	undefined 
*/
function get_port_command_line(){	
	let idx_port = process.argv.indexOf('--port');
	//si existe y se encuentra dentro de los limites del array
	if(idx_port > -1 && idx_port+1 < process.argv.length){
		  	return Number(process.argv[idx_port+1]);
	} 
}


function valid_options(){
	//console.log("Validating options...");
	return true;
}



module.exports.get_global_options = get_global_options; 



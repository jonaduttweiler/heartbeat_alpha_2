var assertions = require('../assets/assertions.js').Assertions;

/*Funcion para comprobar las aserciones que se tienen sobre las respuestas esperadas
	Precondiciones: Se tiene una respuesta con un status code http 200

*/
module.exports.check_assertions = function(request){

	let url = request.host+request.path;
	let assertion = assertions[url]; //TODO: Refact, this should be a portable file as JSON
	let not_match_cause="";
	let match = true;

	if(typeof assertion == "undefined"){
		//console.log(" -- No assertion for "+ url);
		return;
	}

	if(assertion && assertion["content-type"] && assertion["content-type"].indexOf(request.response_headers["content-type"]) == -1){
		match = false;
		not_match_cause="Not match content-type";

//		console.log(assertion["content-type"]+"!="+request.response_headers["content-type"]);
	} else if(assertion && assertion["content-length"] && request.response_headers["content-length"] && assertion["content-length"] != request.response_headers["content-length"]){
		match = false;
		not_match_cause="Not match content-length";
		not_match_cause+=" Expected: "+assertion["content-length"]+"; Received:"+request.response_headers["content-length"];
	}else if(assertion["content-type"] == "application/json"){

		let json_response = request.response_data;

		if (assertion["json-type"] == "array" && !Array.isArray(json_response)){
			match = false;
			not_match_cause="Not match json-type array";
		}
		if (match && assertion["json-type"] == "object" && (typeof json_response !="object" && json_response == null)){
			match = false;
			not_match_cause="Not match json-type object";
		}

		if (match && assertion["json-length"]){
			var response_length = Array.isArray(json_response)? json_response.length : Object.keys(json_response).length;
			if(assertion["json-length"] != response_length){
				match = false;
				not_match_cause="Not match json-length";
			}
		}
	}

	if(!match){
		request.handle_failed_assertion(url,not_match_cause);
	}
}


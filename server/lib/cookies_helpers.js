/**
	Permite obtener un string a partir de un objeto de cookies, el string
	es de la forma cookie1=valor1;cookie2=valor2, lo que nos permite usarlo en un request
*/
module.exports.cookiesObjToStr = function(cookies){
	var cookies_arr = [];
	Object.keys(cookies).forEach(function(key){
		cookies_arr.push(key+"="+cookies[key]);
	});
 	return cookies_arr.join(";");
}


/**
	Permite obtener un objeto a partir de un string de cookies, el string
	es de la forma cookie1=valor1;cookie2=valor2
*/
module.exports.cookiesStrToObj = function (cookies_str){

	var cookies_response = cookies_str.split(";").map(function(e){return e.trim();});
	var cookies_response_object = {};

	cookies_response.forEach(function(pair_value_cookie){
		var splited_cookie = pair_value_cookie.split("=");

		if(splited_cookie.length == 2){
			var cookie_name = splited_cookie[0];
			var cookie_value = splited_cookie[1];
			cookies_response_object[cookie_name] = cookie_value;

		}
	});
	return cookies_response_object;

}


/**
	Permite obtener un objeto a partir de un array de string de cookies, el string
	es de la forma cookie1=valor1;cookie2=valor2
*/
module.exports.cookiesArrStrToObj = function (cookies_arr){
	var cookies_response_object = {};

	cookies_arr.forEach(function(cookies_str){
		var cookies_response = cookies_str.split(";").map(function(e){return e.trim();});

		cookies_response.forEach(function(pair_value_cookie){
			var splited_cookie = pair_value_cookie.split("=");

			if(splited_cookie.length == 2){
				var cookie_name = splited_cookie[0];
				var cookie_value = splited_cookie[1];
				cookies_response_object[cookie_name] = cookie_value;

			}
		});
	});


	return cookies_response_object;

}
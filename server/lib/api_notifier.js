//Permite realizar llamadas a determinadas APIs cuando se dan demoras con algunas de las instancias.
//Momentaneamente solo se implemento para SIGAE WEB LOGIC

const path = require('path');
const root_path = path.dirname(__dirname);//must be /project or /project/server
const http = require('http');
const log = require(`${root_path}/lib/logger.js`).logger_log;
const global_options = require(`${root_path}/lib/options_reader.js`).get_global_options();


function notify_delay(request){

	//Call monitoring glassfish API if a request delay more than the defined threshold.
	if(request.aplication === 'SIGAE WEB LOGIC' && request.time_elapsed_ms > global_options.get_metrics_threshold_ms ){
		let monitoring_glassfish_path = `${global_options.monitoring_glassfish_endpoint}/metric/sigae-i${request.instance}`;
		log("INFO","MONITOR",`--> ${monitoring_glassfish_path}`);

		//Como no vamos a ver las metricas acá, con que nos devuelva un status code 200 es suficiente
		http.get(monitoring_glassfish_path, (resp) => {
			let data = '';
		  // A chunk of data has been recieved.
		  resp.on('data', (chunk) => {
		  	data += chunk;
		  });

		  // The whole request has been received. Print out the result.
		  resp.on('end', () => {
		  	if(global_options.print_metrics){
		  		console.log(JSON.parse(data));	
		  	}
		  	log("INFO","MONITOR",`[OK] <-- ${monitoring_glassfish_path}`);
		  });

		}).on("error", (err) => {
			console.log("Error: " + err.message);
		});
	}
}
  			  		


/*############################################################################
 	MODULES EXPORTS
############################################################################*/

if(global_options.get_metrics_threshold_ms && global_options.monitoring_glassfish_endpoint){
	module.exports.notify_delay = notify_delay; 	
} else {
	log("INFO","BOOTSTRAP","Notify delay disabled");
	module.exports.notify_delay = () => {}; 	
}


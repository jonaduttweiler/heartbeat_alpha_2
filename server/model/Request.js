const url = require('url');
const path = require('path');
const root_path = path.dirname(__dirname);//must be /project or /project/server

const cookies_helpers = require('../lib/cookies_helpers');
const cookiesObjToStr = cookies_helpers.cookiesObjToStr;
const cookiesStrToObj = cookies_helpers.cookiesStrToObj;
const cookiesArrStrToObj = cookies_helpers.cookiesArrStrToObj;

const log = require(`${root_path}/lib/logger.js`).logger_log;

"use strict"
class Request {

	static get ID(){
		if(!this.current_id){
			this.current_id = 1;
		} else {
			this.current_id++;
		}
		return this.current_id;
	} 

	static SET_HANDLER(event_name,handler){
		if(!this.handlers){
			this.handlers = [];
		}
		this.handlers[event_name] = handler;
	}

	constructor(instance){

		let { aplicacion,protocol,host,path,cookie_name,cookie_value,instance_number,port} = instance;
		
		this.id = Request.ID; //TODO: esto me parece que lo necesita solamente el frontend
		this.aplication = aplicacion;
		
		this.host = host;
		this.path = path;
		if(cookie_name && cookie_value){
			this.cookies_obj = {[cookie_name]: cookie_value};
			this.cookies = cookiesObjToStr(this.cookies_obj);	
		} else { //TODO: REFACT. No es necesario especificar esto, los metodos que utilicen la clase deberian manejarlo
			this.cookies_obj = {}; //deberia ser undefined
			this.cookies = "";	//deberia ser undefined
		}

		if(protocol){
			this.protocol = protocol;
		} else {
			this.protocol = "https"; //Default
		}


        if(port){
            this.port = port;
        } else if(this.protocol == "http"){
            this.port=80;
        } else if(this.protocol == "https"){
            this.port=443;       
        } 

        /*Determinamos el numero de instancia, buscando primero si existe la propiedad instance_number, sino 
          busca en las cookies, y por ultimo busca numeros en el nombre del host.
          El numero de instancia es de tipo Number
        */
        try{
			if(instance_number != null){
				this.instance = parseInt(instance_number);
			} else if(cookie_value) {
				this.instance = parseInt(cookie_value.match(/\d+$/)[0]);  
			} else { 
				this.instance = parseInt(host.match(/\d+/g)[0]); 
			}
		  } catch(e){
          	log("TRACE","Request",`The instance number for ${host} could not be determined `); 
          this.instance = 0;
        }


		this.status="created";
		this.summary="";

		/**
		  In addition to the previous properties, a request has the following properties (populates throug if lifecycle)
		   status_code
		   status
		   response_headers
		   response_data
		   summary
		   timing:{
			   send_date
			   time_elapsed_ms
			   dns_time
			   tcp_connection_time
			   tls_connection_time
			   transfer_time
			}
			match_assertions
			assertions_not_match_cause
			*/
	}

	//start_time:Efecctively the timestamp when the backend server makes the request
	mark_as_sent(timestamp){
		this.status = "sent";
		this.start_time = timestamp;
		
		if(Request.handlers["sent"]){
			Request.handlers["sent"](this);
		}
	}

	get_url(){
		return this.url=this.protocol+"://"+this.host+":"+this.port+this.path;
	}
	

	toString(){
		
		let str;
		let url = this.get_url();
		let url_slice = (url.length> 50)? url.slice(0,50)+"..." : url;
		let response_cookies = null;

		if(this.response_headers && this.response_headers['set-cookie']) {
			response_cookies = JSON.stringify(this.response_headers['set-cookie']);	
		}

		switch(this.status){
			case "created":
					str= url_slice+" [cookies:"+this.cookies+"]";
					break;
			case "sent": 
					str= "--> [GET] " + url_slice+" [cookies:"+this.cookies+"]";
					break;
			case "success":
					str= `<-- [${this.status}] [statusCode:${this.status_code}] ${url_slice}`;
					break;
			case "warning":
					str= `<-- [${this.status}] [statusCode:${this.status_code}] ${url_slice}`;
					break;
			case "error":
					str= `<-- [${this.status}] [statusCode:${this.status_code}] ${url_slice}`;
					break;
			case "error_connection":
					str= `<-- [${this.status}] [statusCode:${this.status_code}] ${url_slice}`;
					break;
		}

		if(response_cookies){
			str+= `[Response cookies: ${response_cookies}]`;
		}

		return str;
	}


	/**
		Process response headers of res.
		Results of processing are stored as response properties
		Updates the request with the result of http/s request
		*/
		process_response_headers(res){

			this.status_code = res.statusCode;
			this.response_headers = res.headers;  
			let status_str;

			switch(parseInt(res.statusCode/100)){
		   	case 2:{ // Successful 2xx
		   		status_str = "success";
		   		break;
		   	}
		   	case 3:{//Redirection 3xx
		   		status_str = "warning";
		   		break;
		   	}
		   	case 4:{//Client Error 4xx
		   		status_str = "error";
		   		break;
		   	}
		   	case 5:{//Server Error 5xx
		   		status_str = "error";
		   		break;
		   	}
		   }

		   this.status = status_str;
		   this.process_cookies_response();
		};


	//Si las cookies de la response tienen distintos valores para las cookies del request
	//que se usan para referenciar a una instancia indicamos que la response es del tipo redirect
	//Returns: void. It modifies response object properties status, and summary
	process_cookies_response(){
		if(this.response_headers['set-cookie']){
			let response_cookies = cookiesArrStrToObj(this.response_headers['set-cookie']);
			
			Object.keys(response_cookies).forEach(function(cookie_name){
				//Si devuelve un set cookie para alguna de las cookies que pasamos
				if(this.cookies_obj[cookie_name] && this.cookies_obj[cookie_name] != response_cookies[cookie_name]){
					this.changed_cookies = "set-cookie "+cookie_name+"="+response_cookies[cookie_name]; 
					this.summary += ` changed cookies : ${this.changed_cookies} `;
					//this.status= "cookies_redirect";// Esto no hace un redirect, solamente dice que cambiaron las cookies
				}
			},this);
		}
	};


	/**
		Post data process.
		This function should be called at the end of receive data
	*/
	process_response_data(){
	/*En el caso de sigae logic, hemos modificado una de las urls para que devuelva un json 
	donde uno de los campos indica el tiempo que llevo hacer una consulta (find) a la bd MEDB
	a través del entity mangager de hibernate*/
	if(this.application=="SIGAE WEB LOGIC"){
  	  if(this.response_headers['content-type'] == 'application/json' && this.response_data){
		if(this.response_data["a"] && this.response_data["a"]["response"] && this.response_data["a"]["response"]["indocumentado"]){
				this.db_time_elapsed = this.response_data["a"]["response"]["indocumentado"];
			}
		}
	  }
	}

	calculate_timing(timestamps){
		let {stamp1,stamp3,stamp_lookup,stamp_tcpConnectionAt,stamp_tlsHandshakeAt,stamp_readable} = timestamps;
		let timing = {};	
		timing.send_date = stamp1;
        timing.time_elapsed_ms = stamp3-stamp1;

        //Timing data
		timing.dns_time = stamp_lookup - stamp1;
		timing.tcp_connection_time = stamp_tcpConnectionAt - stamp_lookup;

		if(stamp_tlsHandshakeAt){
			timing.tls_connection_time = stamp_tlsHandshakeAt - stamp_tcpConnectionAt;
			timing.transfer_time = stamp_readable - stamp_tlsHandshakeAt;
		} else {
			timing.transfer_time = stamp_readable - stamp_tcpConnectionAt;
		}

		//this.start_time = stamp1;
		this.timing = timing;
		//TODO: For compatibility with other systems the following properties are included.
		//These represents the same data including in time. Once dependencies are updated, please remove
		//this data redundancy.
		this.time_elapsed_ms = timing.time_elapsed_ms; 
	}



	//If any error is encountered during the request (be that with DNS resolution,
	// TCP level errors, or actual HTTP parse errors) an 'error' event is emitted on the returned request object.
	//So probably that event is only emitted when there is an error at the protocol/connection level.

	register_connection_error(e){
	    this.has_error = true;
        this.error = e;
        this.summary = "Protocol / Connection error";
        this.status = "error_connection";
	}


	handle_failed_assertion(url, cause){
		this.summary += " Not match assertion:"+cause;
		this.match_assertions = false;
		this.assertions_not_match_cause = cause;
		console.log(`[WARNING][ASSERTION FAILED]: ${url} [CAUSE]: ${cause}`);		
	}

	
}

module.exports = Request;